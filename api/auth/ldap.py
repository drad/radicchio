#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import importlib
import logging

import ldap3
from config.config import (
    LDAP_APP_GROUP_PREFIX,
    LDAP_BASE_DN,
    LDAP_BIND_USER_DN,
    LDAP_BIND_USER_PASSWORD,
    LDAP_HOST,
    LDAP_PORT,
    LDAP_USE_SSL,
    LDAP_USE_STARTTLS,
    LDAP_USER_CHECK_FOR_ENABLED,
    LDAP_USER_ROLE_ATTRIBUTES,
    LDAP_USER_ROLE_BATCH,
    LDAP_USER_ROLE_SEARCH_BASE,
    LDAP_USER_ROLE_SEARCH_FILTER,
    LDAP_USER_SEARCH_BASE,
    LDAP_USER_SEARCH_FILTER,
)
from fastapi import HTTPException
from ldap3 import ALL, DEREF_ALWAYS, SUBTREE, Connection, Server
from users.models import UserExt
from users.utils import add_user, get_by_username, update_user

logger = logging.getLogger("default")

server = Server(
    host=LDAP_HOST,
    port=LDAP_PORT,
    use_ssl=LDAP_USE_SSL,
    get_info=ALL,
)

_accounts_spec = importlib.util.find_spec("accounts")
app_has_accounts = _accounts_spec is not None


def get_roles(username):
    """
    Get roles for a user binding as the LDAP_BIND_USER.
    Return: list of roles that the user has (empty if no roles).
    """
    ldap_groups = []
    try:
        conn = Connection(
            server,
            LDAP_BIND_USER_DN,
            LDAP_BIND_USER_PASSWORD,
        )

        if LDAP_USE_STARTTLS:
            logger.debug("- attempting to upgrade connection to TLS...")
            conn.start_tls()

    except ldap3.core.exceptions.LDAPSocketOpenError as e:
        logger.error(f"ERROR Connecting to Server: {server}\n>{e}")
        return None
    except Exception as e:
        logger.error(f"ERROR: LDAP Server connection issue: {e}")
        return ldap_groups

    # perform the Bind operation
    if not conn.bind():
        logger.error(
            f"ERROR: in connecting as LDAP_BIND_USER_DN, please ensure LDAP is setup correctly: {conn.result}"
        )
        return ldap_groups

    # get all of the groups the user is a member of (note: filtered to *{LDAP_APP_GROUP_PREFIX}
    search_filter = LDAP_USER_ROLE_SEARCH_FILTER.format(
        username=username,
        LDAP_BASE_DN=LDAP_BASE_DN,
        LDAP_APP_GROUP_PREFIX=LDAP_APP_GROUP_PREFIX,
    )
    logger.debug(
        f"- roles lookup base={LDAP_USER_ROLE_SEARCH_BASE} - filter={search_filter} - attributes={LDAP_USER_ROLE_ATTRIBUTES}"
    )
    conn.search(
        search_base=LDAP_USER_ROLE_SEARCH_BASE,
        search_filter=search_filter,
        attributes=[LDAP_USER_ROLE_ATTRIBUTES],
    )

    # ~ logger.debug(f"- conn.entries={conn.entries}")
    for entry in conn.entries:
        # ~ logger.debug(f"  - entry={entry}")
        for roles in entry:
            # ~ logger.debug(f"    - roles={roles}")
            for role in roles:
                # ~ logger.debug(f"      - role={role}")
                # LDAP_USER_ROLE_BATCH: glauth has roles in a batch (as you would expect) but openldap-* does not.
                if LDAP_USER_ROLE_BATCH:
                    if role.startswith(f"ou={LDAP_APP_GROUP_PREFIX}"):
                        # ~ logger.debug(f"        - keep role={role}")
                        # we need role as admins, users, etc. not trax_admins, trax_users, etc.
                        role_ou = role.split(",")[0]
                        role_name = role_ou.split("=")[1]
                        role_cleaned = role_name.replace(LDAP_APP_GROUP_PREFIX, "")
                        ldap_groups.append(role_cleaned)
                else:
                    # this is the old 'ldap' structure.
                    role_cleaned = str(role).replace(LDAP_APP_GROUP_PREFIX, "")
                    ldap_groups.append(role_cleaned)

    return ldap_groups


def bind(username, password) -> UserExt:
    """
    Bind (authenticate) as user.
    """

    logger.debug(f"- bind as user={username}")
    conn = Connection(
        server,
        f"cn={username},{LDAP_BASE_DN}",
        password,
    )
    if LDAP_USE_STARTTLS:
        logger.debug("- attempting to upgrade connection to TLS...")
        conn.start_tls()

    logger.debug("- binding user...")
    # perform the Bind operation
    if not conn.bind():
        logger.critical(f"Authentication issue: {conn.result}")
        raise HTTPException(
            status_code=401, detail=f"Authentication issue: {conn.result}"
        )

    # get user's details
    user_base = LDAP_USER_SEARCH_BASE.format(
        username=username, LDAP_BASE_DN=LDAP_BASE_DN
    )
    user_filter = LDAP_USER_SEARCH_FILTER.format(username=username)
    logger.debug(f"- user search base={user_base} - filter={user_filter}")
    attributes = ["cn", "sn", "displayName", "givenName", "mail"]
    if LDAP_USER_CHECK_FOR_ENABLED:
        attributes.append("accountStatus")
    conn.search(
        search_base=user_base,
        search_filter=user_filter,
        search_scope=SUBTREE,
        dereference_aliases=DEREF_ALWAYS,
        attributes=attributes,
    )
    logger.debug(f"- user search response: {conn.response}")
    response = conn.response[0]
    if not response:
        logger.critical(f"Could not retrieve user details: {conn.response}")
        raise HTTPException(
            status_code=401, detail=f"Authentication retrieval issue: {conn.response}"
        )
    else:
        # build user object here...
        logger.debug(f"- ldap response attributes: {response['attributes']}")
        # if account is disabled then block access.
        #   note: some ldap systems may not have the accountStatus (enabled/disabled) logic.
        if (
            "accountStatus" in response["attributes"]
            and response["attributes"]["accountStatus"][0] != "active"
        ):
            logger.critical("- ACCOUNT HAS BEEN DISABLED!")
            raise HTTPException(
                status_code=401,
                detail=f"User account is disabled - username={username}",
            )
        username = response["attributes"]["cn"][0]
        first_name = (
            response["attributes"]["givenName"][0]
            if "givenName" in response["attributes"]
            and len(response["attributes"]["givenName"]) > 0
            else None
        )
        last_name = (
            response["attributes"]["sn"][0]
            if "sn" in response["attributes"] and len(response["attributes"]["sn"]) > 0
            else None
        )
        display_name = (
            response["attributes"]["displayname"][0]
            if "displayname" in response["attributes"]
            and len(response["attributes"]["displayname"]) > 0
            else None
        )
        email = (
            response["attributes"]["mail"][0]
            if "mail" in response["attributes"]
            and len(response["attributes"]["mail"]) > 0
            else None
        )
        # NOTICE: we do not set items that are not in ldap (note, etc.)
        #         as this allows the app to 'override' those items.
        user = UserExt(
            username=username,
            display_name=display_name,
            first_name=first_name,
            last_name=last_name,
            email=email,
        )
        return user


async def login(username, password):
    """
    Authenticate against ldap
    """

    # check users roles.
    #  note: we check roles before authenticating user.
    roles = get_roles(username)
    logger.debug(f"- user roles: {roles}")
    if not roles:
        logger.warning(
            f"No applicable roles found (LDAP_APP_GROUP_PREFIX={LDAP_APP_GROUP_PREFIX}), user must have roles to login. Cannot continue."
        )
        raise HTTPException(status_code=401, detail="User has no roles")
    # check to ensure user has at least one account.

    # bind (authenticate) as user.
    user = bind(username, password)
    # ~ logger.debug(f"- user is: {user}")

    if not user:
        logger.error("Bind user issue, no user returned.")
        raise HTTPException(status_code=401, detail="User bind failed")
    else:
        user.roles = roles
        # ~ logger.debug("- looking up user...")
        existing_user = await get_by_username(username)
        # ~ logger.debug(f" - existing_user: {existing_user}")
        if existing_user:
            # NOTE: we allow admins in without any accounts so they can set up accounts.
            if (
                app_has_accounts
                and len(existing_user["accounts"]) < 1
                and "admins" not in roles
            ):
                logger.critical(
                    f"User has no Accounts and is not an admin.\n  - user: {user}"
                )
                raise HTTPException(status_code=401, detail="User has no accounts")
            # NOTICE: accounts, notes, and disabled are retained from user account if it exists.
            logger.debug(f" - username: {username} exists, updating...")
            user.enabled = (
                existing_user["enabled"] if existing_user["enabled"] else False
            )
            if app_has_accounts:
                user.accounts = (
                    existing_user["accounts"] if existing_user["accounts"] else []
                )
            user.note = existing_user["note"] if existing_user["note"] else None
            user.avatar = (
                existing_user["avatar"] if existing_user["avatar"] else user.avatar
            )
            user.display_name = (
                existing_user["display_name"]
                if existing_user["display_name"]
                else user.display_name
            )
            # update existing user
            r = await update_user(user)
            logger.debug(f" - update_user response: {r}")
        else:
            logger.debug(f" - username: {username} does not exist, adding...")
            # create new user
            r = await add_user(user)
            logger.debug(f" - add_user response: {r}")

        return user, roles
