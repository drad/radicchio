#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging

import aiocouch
from _common.utils import fix_id
from config.config import couchdb
from fastapi import HTTPException
from notifiers.models import _db

logger = logging.getLogger("default")


async def get_notifier_by_id(_id: str = None):
    """
    Get account by id
    """

    logger.debug(f"- get notifier by id: {_id}")
    db = await couchdb[_db.value]
    try:
        return fix_id(await db[_id])
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")
