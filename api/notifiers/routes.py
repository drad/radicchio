#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import inspect
import logging
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id, generate_id, get_route_roles, make_id
from accounts.models import Account
from accounts.utils import get_account, get_account_by_id
from auth.utils import has_account_access
from config.config import couchdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from notifiers.models import Notifier, NotifierBase, NotifierConfig, _db
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
notifiers_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id}")
    try:
        doc = await db[_id]
        return fix_id(doc)
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


# @TODO: we may need a get all for user???
@notifiers_router.get("/", response_model=List[Notifier])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    db = await couchdb[_db.value]

    selector = {}
    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        docs.append(fix_id(doc))

    return docs


@notifiers_router.get("/one/{id}", response_model=Notifier)
async def get_by_id(
    _id: str = Query(..., description="The id of the document to get"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get one by id
    """

    db = await couchdb[_db.value]
    doc = await _get_or_404(_id, db)
    account = Account(**await get_account_by_id(doc["account"]))
    logger.debug(f"- doc: {doc}")
    role = get_route_roles(inspect.currentframe())
    logger.debug(f"- role: {role}")

    # check to ensure use has access for account.
    if not await has_account_access(current_user, account, roles=role):
        raise HTTPException(status_code=401, detail="Not authorized")

    logger.debug(f"- returning doc={doc}")

    return doc


@notifiers_router.get("/by_name/", response_model=Notifier)
async def get_by_name(
    account: str = Query(..., description="account to get"),
    name: str = Query(..., description="name to get"),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get by name and account
    """

    db = await couchdb[_db.value]

    _account = await get_account(name=account)
    # ~ logger.debug(f"- account: {_account}")
    role = get_route_roles(inspect.currentframe())
    if not await has_account_access(current_user, _account, roles=role):
        raise HTTPException(status_code=401, detail="Not authorized")

    selector = {
        "name": name,
        "account": _account.id_,
    }
    # ~ logger.debug(f"- selector: {selector}")

    docs = []
    async for doc in db.find(selector=selector, limit=10, skip=0):
        docs.append(fix_id(doc))

    if len(docs) == 1:
        return docs[0]
    elif len(docs) < 1:
        raise HTTPException(status_code=404, detail="Not found")
    else:
        raise HTTPException(status_code=400, detail="Too many found")


@notifiers_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    obj: NotifierBase,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    """

    db = await couchdb[_db.value]

    # check to ensure use has access for account
    _account = await get_account(name=obj.account)
    role = get_route_roles(inspect.currentframe())
    # ~ logger.debug(f"- account: {_account}")
    if not await has_account_access(current_user, _account, roles=role):
        raise HTTPException(status_code=401, detail="Not authorized")

    _notifier = NotifierBase(
        name=obj.name,
        account=_account.id_,
        kind=obj.kind,
        config=obj.config,
        note=obj.note,
        disabled=obj.disabled,
    )

    try:
        doc_id = make_id(items=[generate_id()])
        logger.debug(f"- doc_id is: {doc_id}")
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_notifier),
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        raise HTTPException(
            status_code=409, detail=f"Duplicate Key: cannot add '{doc_id}'."
        )


@notifiers_router.put(
    "/",
    response_model=DocInfo,
)
async def update(
    account: str = Query(..., description="account to get"),
    name: str = Query(..., description="name to get"),
    config: NotifierConfig = None,
    disabled: bool = False,
    note: str = None,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update by name and account
    - note: cannot update name, account or kind (need to delete and re-add)
    """

    # check to ensure use has access for account
    _account = await get_account(name=account)
    role = get_route_roles(inspect.currentframe())
    # ~ logger.debug(f"- account: {_account}")
    if not await has_account_access(current_user, _account, roles=role):
        raise HTTPException(status_code=401, detail="Not authorized")

    doc = await get_by_name(name=name, account=account, current_user=current_user)
    logger.debug(f"- updating doc: {doc}")
    doc["config"] = config.dict() if config else doc["config"]
    doc["disabled"] = disabled if disabled else doc["disabled"]
    doc["note"] = note if note else doc["note"]
    # ~ doc["updated"] = jsonable_encoder(datetime.now())
    # ~ doc["updator"] = current_user.username
    await doc.save()

    return await doc.info()


@notifiers_router.delete("/", response_model=DocInfo)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard delete by id
    """

    db = await couchdb[_db.value]
    resp = DocInfo(ok="ok", id=_id, rev="", msg="deleted")
    doc = await db[_id]

    # check to ensure use has access for account
    _account = Account(**await get_account_by_id(doc["account"]))
    role = get_route_roles(inspect.currentframe())
    # ~ logger.debug(f"- role: {role}")
    # ~ logger.debug(f"- account: {_account}")
    if not await has_account_access(current_user, _account, roles=role):
        raise HTTPException(status_code=401, detail="Not authorized")

    dr = await doc.delete(discard_changes=True)
    # note: dr will be None if delete succeeds.
    if dr:
        resp.ok = "fail"
        resp.msg = dr

    return resp
