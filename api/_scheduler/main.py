#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from typing import List

import arrow
import graypy
import pytz
from _common.models import RunInterval
from apscheduler.jobstores.base import JobLookupError
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from arq import create_pool
from arq.connections import RedisSettings
from config.config import (
    API_PATH,
    APP_LOGLEVEL,
    DEPLOY_ENV,
    GRAYLOG_HOST,
    GRAYLOG_PORT,
    LOG_TO,
    REDIS_DB_APS,
    REDIS_DB_ARQ,
    REDIS_HOST,
    REDIS_PORT,
    REDIS_PWD,
    SCHEDULER_VALIDATE_MONITOR,
    cfg,
)
from fastapi import FastAPI, Query
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from monitors.models import MonitorBase
from monitors.utils import get_monitor_by_id

scheduler = None
redis = None
app_name = f"{cfg.core.name}-scheduler"

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(APP_LOGLEVEL))
graylog_handler = graypy.GELFUDPHandler(host=GRAYLOG_HOST, port=GRAYLOG_PORT)
console_handler = logging.StreamHandler()
if "graylog" in LOG_TO:
    logger_base.addHandler(graylog_handler)
if "console" in LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": app_name, "application_env": DEPLOY_ENV},
)

if APP_LOGLEVEL.upper() == "DEBUG":
    logger.info("### App Started With Debug On ###")

logger.info(
    f"{app_name} - v.{cfg.core.version} ({cfg.core.modified}) - {APP_LOGLEVEL} - {LOG_TO}"
)
logger.info(f"- Scheduler DB: {REDIS_HOST}:{REDIS_PORT}/{REDIS_DB_APS}")
logger.info(f"- Worker DB:    {REDIS_HOST}:{REDIS_PORT}/{REDIS_DB_ARQ}")

app = FastAPI(
    title=f"{app_name}",
    description=f"{cfg.core.description}",
    version=f"{cfg.core.version}",
    openapi_url=f"{API_PATH}/openapi.json",
    docs_url=f"{API_PATH}/docs",
    redoc_url=None,
)


async def monitor(monitor_id: str = None):
    """
    Create an ARQ task to perform the monitor
    """

    dts = arrow.utcnow().to("America/New_York").format("YYYY-MM-DD HH:mm:ss")
    logger.debug(f"{dts}: add task for monitor: {monitor_id}")

    await redis.enqueue_job("monitor", monitor_id)


@app.on_event("startup")
async def startup_event():
    global scheduler, redis

    # get redis connection for arq
    redis = await create_pool(
        RedisSettings(
            host=REDIS_HOST,
            port=REDIS_PORT,
            database=REDIS_DB_ARQ,
            password=REDIS_PWD,
        )
    )

    scheduler = AsyncIOScheduler(timezone=pytz.utc)
    scheduler.add_jobstore(
        "redis",
        host=REDIS_HOST,
        port=REDIS_PORT,
        db=REDIS_DB_APS,
        password=REDIS_PWD,
    )
    scheduler.start()


@app.get("/", response_model=List[dict])
async def list_jobs():
    """
    List all jobs
    """

    jl = []
    jobs = scheduler.get_jobs()
    # ~ logger.info(jobs)
    for job in jobs:
        # ~ logger.info(f"- job: {job}")
        jl.append(
            {
                "id": job.id,
                "name": job.name,
                "trigger": str(job.trigger),
                "next_run_time": job.next_run_time,
                "executor": job.executor,
                "func": job.func,
                "args": job.args,
                "kwargs": job.kwargs,
                "coalesce": job.coalesce,
                "misfire_grace_time": job.misfire_grace_time,
                "max_instances": job.max_instances,
            }
        )

    json_compatible_item_data = jsonable_encoder(jl)
    return JSONResponse(content=json_compatible_item_data)


@app.post(
    "/schedule-monitor",
    response_model=dict,
)
async def schedule_monitor(
    monitor_id: str = Query(..., description="id of the monitor"),
    run_interval: RunInterval = None,
):
    """
    Endpoint used by API to schedule a monitor
    """

    logger.info(
        f"- scheduling monitor [{monitor_id}] with run_interval: {run_interval}"
    )
    if SCHEDULER_VALIDATE_MONITOR:
        # Lookup monitor first to ensure it is a valid monitor and that it is enabled.
        #   NOTICE: if SCHEDULER_VALIDATE_MONITOR=false then the monitor is not validated nor do we check if monitor is disabled.
        _monitor = await get_monitor_by_id(monitor_id=monitor_id)
        if _monitor:
            m = MonitorBase(**_monitor)
            if m.disabled:
                return {"success": False, "message": "monitor disabled"}
        else:
            return {"success": False, "message": "monitor not found"}

    scheduler.add_job(
        monitor,
        "interval",
        seconds=run_interval.seconds,
        minutes=run_interval.minutes,
        hours=run_interval.hours,
        days=run_interval.days,
        weeks=run_interval.weeks,
        timezone=run_interval.timezone,
        start_date=run_interval.start_date,
        end_date=run_interval.end_date,
        args=[monitor_id],
        id=f"monitor_{monitor_id}",
    )
    return {
        "success": True,
        "message": "monitor scheduled",
        "job_id": f"monitor_{monitor_id}",
    }


@app.delete("/{job_id}", response_model=dict)
async def delete_job(job_id: str = Query(..., description="monitor job id")):
    """
    Delete job by job id
    """

    try:
        scheduler.remove_job(job_id)
    except JobLookupError as e:
        # this will happen if you try to remove a job that does not exist e.g. delete of a monitor where the monitor did not have a job or job was disabled.
        logger.debug(f"Warning: job was not found, cannot remove: {e}")
    except Exception as e:
        msg = f"ERROR removing job: {e}"
        logger.error(msg)
        return {"success": False, "message": msg}

    return {"success": True, "message": "job deleted"}
