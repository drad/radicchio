#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from typing import List

import aiocouch
from _common.models import DocInfo
from _common.utils import fix_id
from config.config import couchdb
from fastapi import APIRouter, Depends, HTTPException, Query, Security
from fastapi.encoders import jsonable_encoder
from pydantic import HttpUrl
from users.models import (
    User,
    UserAccount,
    UserBase,
    _db,
    get_current_active_user,
    get_current_user,
)
from users.utils import fix_doc

logger = logging.getLogger("default")
users_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@users_router.get("/me", response_model=UserBase)
async def me(current_user: UserBase = Depends(get_current_active_user)):
    """
    Get current user info.
    """

    return current_user


@users_router.get("/", response_model=List[User])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    # @TODO: add limit and skip logic.
    db = await couchdb[_db.value]

    docs = []
    async for doc in db.docs():
        docs.append(fix_id(doc))

    return docs


# NOTICE: we do not need an 'add' route in api as users are added through
#   ldap; however, there is a non-exposed add in utils.py.


@users_router.get("/{username}", response_model=User)
async def get_one(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """Get by username"""

    db = await couchdb[_db.value]

    return await fix_doc(await _get_or_404(username, db))


# 2020-07-01:drad update of user is limited to updating the following as other items are set via ldap:
#   - disabled: this will disable the user in the app
#   - note: an app specific note for the user
#   - accounts: a list of accounts the user belongs to
@users_router.put(
    "/{username}",
    response_model=dict,
)
async def update(
    username: str = Query(..., description="username of user to update"),
    enabled: bool = Query(True, description="Is the user enabled?"),
    display_name: str = Query(None, description="name used in application display"),
    avatar: HttpUrl = Query(None, description="user's avatar"),
    note: str = Query(None, description="note on user"),
    accounts: List[UserAccount] = None,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update
    NOTE: accounts that do not exist will be removed from request prior to save.
    """

    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    # logger.debug(f"- updating user;\n  ⟶ preferences: {preferences}\n  ⟶ credentials: {credentials}")
    doc["enabled"] = enabled
    doc["note"] = note if note else doc["note"]
    doc["display_name"] = display_name if display_name else doc["display_name"]
    doc["avatar"] = avatar if avatar else doc["avatar"]
    doc["accounts"] = (jsonable_encoder(accounts) if accounts else doc["accounts"],)

    await doc.save()

    return await doc.info()


@users_router.delete("/{username}", response_model=dict)
async def delete(
    username: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Hard Delete by id
    """

    resp = DocInfo(ok="ok", id=username, rev="", msg="deleted")
    db = await couchdb[_db.value]
    doc = await _get_or_404(username, db)
    dr = await doc.delete()
    # note: dr will be None if delete succeeds.
    if dr:
        resp.ok = "fail"
        resp.msg = dr

    return resp
