#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

from datetime import datetime
from enum import Enum
from typing import Optional

from config.models import Cdb
from pydantic import BaseModel, HttpUrl, Json

_db = Cdb.RESULTS


class ResultStatus(str, Enum):
    success = "success"
    failure = "failure"


class CertData(BaseModel):

    # ~ ssl_expired: Optional[bool] = False
    valid: Optional[bool] = False  # previously called 'ssl_expired'
    # ~ ssl_expires: Optional[datetime] = None
    expire_date: Optional[datetime] = None  # previously called 'ssl_expires'
    # ~ ssl_past_notify: Optional[bool] = False
    past_notify: Optional[bool] = False  # previously called 'ssl_past_notify'


class ResultBase(BaseModel):
    """Base"""

    code: Optional[
        str
    ] = None  # http response code of result (note: will be null for cert check results)
    url: Optional[
        HttpUrl
    ] = None  # note: this is the response url which may differ from the url of the monitor (in cases of redirects)
    meta_data: Optional[Json] = None  #
    response_time: Optional[
        float
    ] = None  # time it took for response (status + content)
    status_time: Optional[float] = None  # time it took to get http status
    monitor_time: Optional[
        float
    ] = None  # time it took for monitor (status + content + monitor)
    error: Optional[str] = None  # error message (if any)
    status: ResultStatus = None
    cert_data: Optional[CertData] = None
    note: Optional[str] = None  # used to add a note about a result.


class ResultExt(ResultBase):
    """Extended (added by backend logic)"""

    # note: this needs to be set/overwrote on result instantiation as using
    #  datetime.now() here will only get you now of when worker was started.
    created: datetime = datetime.now()


class Result(ResultExt):
    """Actual (at DB level)"""

    id_: str
