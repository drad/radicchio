#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from datetime import datetime
from typing import List

import aiocouch
import arrow
from _common.utils import fix_id
from accounts.utils import get_account
from config.config import REDIS_DB_ARQ, REDIS_HOST, REDIS_PORT, REDIS_PWD, couchdb
from fastapi import APIRouter, HTTPException, Query, Security
from monitors.utils import get_monitor
from results.models import Result, ResultExt, _db
from results.utils import monitor_results_for_span
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
results_router = APIRouter()


async def _get_or_404(_id):
    """
    Get document by id or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id.lower()}")
    db = await couchdb[_db.value]
    try:
        doc = fix_id(await db[_id.lower()])
        return doc
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@results_router.get("/", response_model=List[Result])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    # ~ logger.debug(f"- get all: current_user: {current_user}")
    db = await couchdb[_db.value]

    selector = {}
    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        docs.append(fix_id(doc))

    return docs


@results_router.get("/for_timespan", response_model=List[ResultExt])
async def get_monitor_results_for_timespan(
    account: str = Query(..., description="account to get"),
    name: str = Query(..., description="name to get"),
    start: datetime = Query(
        None,
        description="start datetime of results (blank for [current datetime - 24hrs])",
    ),
    end: datetime = Query(
        None,
        description="end datetime of results (blank for current datetime)",
    ),
    # limit: int = 10,
    # skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get monitor results for a given timespan.
    """

    if not end:
        # set default end
        end = arrow.utcnow()

    if not start:
        # set default start
        start = end.shift(hours=-24)

    logger.debug(f"- get all for timespan from {start} to {end}")
    monitor = await get_monitor(name=name, account=account)
    logger.debug(f"- found monitor: {monitor}")
    return await monitor_results_for_span(
        monitor_id=monitor["_id"],
        start=start,
        end=end,
    )


@results_router.get("/monitor_results_report/", response_model=dict)
async def monitor_results_report(
    account: str = Query(..., description="account to get"),
    name: str = Query(..., description="name to get"),
    start: datetime = Query(
        None,
        description="start datetime of results (blank for [current datetime - 24hrs])",
    ),
    end: datetime = Query(
        None,
        description="end datetime of results (blank for current datetime)",
    ),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manually initiate the MRR.
    """

    from arq import create_pool
    from arq.connections import RedisSettings

    if not end:
        # set default end
        end = arrow.utcnow()

    if not start:
        # set default start
        start = end.shift(hours=-24)

    redis = await create_pool(
        RedisSettings(
            host=REDIS_HOST, port=REDIS_PORT, database=REDIS_DB_ARQ, password=REDIS_PWD
        )
    )

    # @TODO: check to make sure the user has access to monitor before scheduling.
    # logger.debug(f"getting monitor: {name}")
    _monitor = await get_monitor(name=name, account=account, check_account_access=True)
    logger.debug(
        f"- monitor: {_monitor['id_']} ({type(_monitor['id_'])}) --> {_monitor['name']}"
    )

    acc = await get_account(account)

    # enqueue the monitor detail report for processing by a worker.
    # @todo would like to have separate queues for different types of jobs
    job = await redis.enqueue_job(
        "monitor_results_report",
        str(_monitor["id_"]),
        _monitor["name"],
        acc,
        start,
        end,
        # ~ _queue_name="results:mrr",
    )
    return {
        "success": True,
        "message": f"monitor detail report queued for processing with id: {job.job_id}",
    }
