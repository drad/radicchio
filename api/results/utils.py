#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from datetime import datetime
from typing import List

import aiocouch
import aiohttp
import arrow
from _common.utils import clean_dict, generate_id, make_id
from config.config import CDB_URI, cdb_aiohttp_auth, couchdb
from fastapi.encoders import jsonable_encoder
from results.models import ResultExt, _db

logger = logging.getLogger("default")


async def add_result(
    monitor_id=None,  # monitor.id of result to add
    doc=None,  # the monitor document
):
    """
    Add result
    NOTICE: UNSAFE, no access checks are performed here!
    """

    db = await couchdb[_db.value]
    logger.debug(f"- raw incoming doc: {doc}")
    _result = clean_dict(jsonable_encoder(doc))
    logger.debug(f"- cleaned incoming doc: {_result}")
    try:
        # this db is partitioned, the id has the following format:
        # "partition:docid"
        doc_id = make_id(items=[monitor_id, generate_id()])
        _doc = await db.create(
            doc_id,
            data=_result,
        )
        await _doc.save()
        return await _doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None


async def insert_batch(db=None, data=None):
    """
    Insert a batch of docs. In essence this allows "batch insert" by using the context manager with create_docs,
    each time the context manager is closed one request containing all docs are sent to the db.
    """

    async with db.create_docs() as bulk:
        for item in data:
            # ~ logger.debug(f"- inserting item: {item}")
            bulk.create(item["key"], data=jsonable_encoder(item["data"]))


async def monitor_results_for_span(
    monitor_id: str = None,
    start: datetime = None,
    end: datetime = None,
) -> List[ResultExt]:
    """
    Get results by monitor.
    NOTICE: this uses aiohttp to leverage the tables partition in retrieving data.
    """

    logger.debug(f"- monitor results from {start} to {end}")

    # use aiohttp to get data directly as aiocouch does not (currently) support partition queries
    async with aiohttp.ClientSession() as session:
        params = {
            "selector": {
                "created": {
                    "$gte": arrow.get(start).format("YYYY-MM-DDTHH:mm:ss Z"),
                    "$lt": arrow.get(end).format("YYYY-MM-DDTHH:mm:ss Z"),
                }
            },
            "limit": 999999,
            "sort": [{"created": "asc"}],
        }
        logger.debug(f"params: {params}")
        async with session.post(
            f"{CDB_URI}/results/_partition/{monitor_id}/_find",
            auth=cdb_aiohttp_auth,
            json=params,
        ) as response:
            j = await response.json()

        # note: views return "rows" while find returns "docs"
        data = j["docs"] if "docs" in j else []

    _results = []
    logger.debug(f"- we got {len(data)} monitor results:")
    for result in data:
        # ~ logger.debug(f"- raw result: {result}")
        _result = ResultExt(**result)
        # ~ #logger.debug(f"- parsed result = {_result}")
        _results.append(_result)

    return _results
