#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging

import aiocouch
from _common.utils import fix_id
from config.config import couchdb
from fastapi import HTTPException

from .models import Account, _db

logger = logging.getLogger("default")


async def get_account_by_id(_id: str = None) -> Account:
    """
    Get account by id
    """

    logger.debug(f"get account by id for: {_id}")
    db = await couchdb[_db.value]
    try:
        return fix_id(await db[_id])
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


async def get_account(name: str = None) -> Account:
    """
    Get account by name
    """

    logger.debug(f"- get account by name: {name}")
    db = await couchdb[_db.value]
    selector = {"name": name}

    docs = []
    async for doc in db.find(selector=selector):
        docs.append(fix_id(doc))

    if len(docs) == 1:
        return Account(**docs[0])

    else:
        # ~ logger.debug(f"- found docs: {len(docs)}:\n{docs}")
        logger.error(
            f"ERROR: Unexpected Number of Results! Error in getting account by name, too many matches were found, expecting 1 found [{len(docs)}]: {docs}"
        )
        raise HTTPException(status_code=404, detail="Account not found")


# ~ async def has_account_access(
# ~ current_user: UserBase = None,
# ~ account: Account = None,
# ~ roles: Role = None,

# ~ user_accounts: List = None,
# ~ account: str = None,
# ~ roles: Role = None,
# ~ ) -> bool:
# ~ """
# ~ Check if user has account access
# ~ """

# ~ # get role for account in question.
# ~ logger.debug(f"- current user accounts: {current_user.accounts}")
# ~ logger.debug(f"- account: {account}")
# ~ ac_role = [a.role for a in current_user.accounts if a.id_ == account.id_][0]
# ~ # return if role for account is in role(s) list.
# ~ return ac_role in roles


async def get_accounts(disabled: bool = None, summary_report: bool = None):
    """
    Get all Accounts.
    if additional kwargs are supplied they are applied to the filter to get monitors.
    """

    db = await couchdb[_db.value]
    # _account = await get_account(account)
    # logger.debug(f"- account ({account}) lookup: {_account}")
    selector = {}
    if disabled is not None:
        selector["disabled"] = disabled
    logger.debug(f"- selector: {selector}")

    docs = []
    async for doc in db.find(selector=selector):
        docs.append(fix_id(doc))

    return docs
