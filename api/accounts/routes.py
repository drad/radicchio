#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
from typing import List

import aiocouch
from _common.models import DocInfo, Recipient
from _common.utils import fix_id, generate_id, make_id
from accounts.utils import get_account
from config.config import REDIS_DB_ARQ, REDIS_HOST, REDIS_PORT, REDIS_PWD, couchdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from monitors.utils import update_cert_checks
from users.models import UserBase, get_current_user

from .models import Account, AccountBase, _db

logger = logging.getLogger("default")
accounts_router = APIRouter()


async def _get_or_404(_id, db):
    """
    Get a document or raise a 404
    """

    logger.debug(f"_get_or_404 request on: {_id}")
    try:
        return await db[_id]
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@accounts_router.get("/", response_model=List[Account])
async def get_all(
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get all
    """

    db = await couchdb[_db.value]
    selector = {}

    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        docs.append(fix_id(doc))

    return docs


@accounts_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    obj: AccountBase,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Add new
    """

    _doc = AccountBase(
        name=obj.name,
        summary_report=obj.summary_report,
        recipients=obj.recipients,
        note=obj.note,
        disabled=obj.disabled,
    )

    db = await couchdb[_db.value]

    try:
        doc_id = make_id(items=[generate_id()])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        return await doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None


@accounts_router.get("/{name}", response_model=Account)
async def get_by_name(
    name: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get by name
    """

    return await get_account(name=name)


@accounts_router.put(
    "/",
    response_model=DocInfo,
)
async def update(
    _id: str = Query(..., description="The id of the document to update"),
    name: str = Query(None, description="The name of the item"),
    summary_report: bool = Query(None, description="Enable summary report"),
    recipients: List[Recipient] = None,
    disabled: bool = Query(None, description="Item disabled"),
    note: str = Query(None, description="Note for the item"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Update by name
    """

    db = await couchdb[_db.value]

    doc = await _get_or_404(_id, db)
    logger.debug(f"updating doc: {doc}")

    doc["name"] = name if name else doc["name"]
    doc["summary_report"] = summary_report if summary_report else doc["summary_report"]
    doc["recipients"] = (
        jsonable_encoder(recipients) if recipients else doc["recipients"]
    )
    doc["disabled"] = disabled if disabled else doc["disabled"]
    doc["note"] = note if note else doc["note"]

    logger.debug(f"doc before save: {doc}")
    await doc.save()

    return await doc.info()


@accounts_router.delete("/{name}", response_model=dict)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Delete an Account by Name
    """

    db = await couchdb[_db.value]

    resp = DocInfo(ok="ok", id=_id, rev="", msg="deleted")
    _doc = await _get_or_404(_id, db)
    _dr = await _doc.delete()
    # note: dr will be None if delete succeeds.
    if _dr:
        resp.ok = "fail"
        resp.msg = _dr

    return resp


@accounts_router.get("/monitor_summary_report/", response_model=dict)
async def manually_initiate_monitor_summary_report(
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manually initiate the MSR by iterating over all accounts for the current user
    and calling the 'monitor_summary_report' job for each monitor in the account.

    __NOTE: only Accounts which are not disabled and summary_report is enabled will be included__

    __NOTE: only Monitors which are not disabled and summayr_report is enabled will be included__
    """

    from arq import create_pool
    from arq.connections import RedisSettings

    redis = await create_pool(
        RedisSettings(
            host=REDIS_HOST, port=REDIS_PORT, database=REDIS_DB_ARQ, password=REDIS_PWD
        )
    )
    logger.info("processing monitor summary report (manual)")

    # get all accounts with summary_report enabled that are not disabled.
    #  NOTICE: upon moving to couchdb we send all to worker which then determines
    #   if the account should be ran (not disabled and summary_report enabled).
    for account in await get_all(limit=9999, skip=0, current_user=current_user):
        logger.info(f"enqueue summary report task for account: {account['name']}")
        # enqueue job so worker(s) can perform the summary report.
        job = await redis.enqueue_job("monitor_summary_report", account["name"])

    ret_msg = f"monitor summary report enqueued with job id: {job.job_id}"
    logger.info(ret_msg)
    return {"success": True, "message": ret_msg}


@accounts_router.get("/monitor_cert_check/", response_model=dict)
async def monitor_cert_check(
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Manually initiate a monitor cert check for all monitors
    """

    return await update_cert_checks()
