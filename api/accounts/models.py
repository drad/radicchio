#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>


from datetime import datetime
from typing import List, Optional

from _common.models import Recipient
from config.models import Cdb
from pydantic import BaseModel

_db = Cdb.ACCOUNTS


class AccountBase(BaseModel):
    """Base"""

    name: str = None
    summary_report: bool = None  # is summary report (MSR) enable?
    recipients: List[Recipient] = []
    note: Optional[str] = None

    disabled: bool = False  # is account disabled?


class AccountExt(AccountBase):
    """Extended (added by backend logic)"""

    created: datetime = datetime.now()


class Account(AccountExt):
    """Actual (at DB level)"""

    id_: str
