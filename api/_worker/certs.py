#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging
import ssl

import arrow
import OpenSSL

logger = logging.getLogger("default")


def check(url: str, notify_limit: int):
    """Check TLS Certs"""

    from urllib.parse import urlsplit

    current_date = arrow.utcnow()
    logger.info(
        f"checking certificate as of: {current_date.format('YYYY-MM-DD HH:mm')}"
    )
    valid = False
    expire_date = current_date.shift(days=-1).datetime
    past_notify = True

    # use base url (no protocol or path)
    hostname = urlsplit(url).netloc
    port = "443"
    data = None

    try:
        cert = ssl.get_server_certificate((hostname, port))
        data = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)

        logger.debug(f"ssl check data: {data}")
        if data:
            nb = data.get_notBefore().decode("utf-8")
            na = data.get_notAfter().decode("utf-8")
            logger.debug(f"notBefore: {nb}")
            logger.debug(f"notAfter: {na}")
            not_before = arrow.get(nb.replace("Z", ""), "YYYYMMDDHHmmss")
            not_after = arrow.get(na.replace("Z", ""), "YYYYMMDDHHmmss")

            logger.info(
                f"current_date: {current_date} | not_before: {not_before} | not_after: {not_after}"
            )

            valid = (
                True if current_date.is_between(not_before, not_after, "[]") else False
            )
            expire_date = not_after.datetime

            # check if expires is in alert range, if so notify.
            notify_date = arrow.utcnow().shift(days=+notify_limit)
            logger.info(f"{url} ssl expire notify date is: {notify_date}")
            # if not_after is within notify range set past_notify.
            past_notify = True if not_after < notify_date else False
        else:
            valid = False

        logger.info(
            f"check_certs returning valid={valid}, expire_date={expire_date}, past_notify={past_notify}"
        )
    except Exception as e:
        logger.error(f"Error occurred with cert check for host={hostname}:\n\t{e}")

    return valid, expire_date, past_notify
