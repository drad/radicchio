#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import asyncio
import logging
from datetime import datetime
from typing import List

import arrow
import graypy
import markdown
from _common.models import DocInfo, Recipient
from _worker.certs import check
from accounts.utils import get_account
from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientConnectorError
from arq import create_pool, cron
from arq.connections import RedisSettings
from config.config import (
    APP_LOGLEVEL,
    ARQ_DAILY_CERT_CHECK_HOUR,
    ARQ_DAILY_CERT_CHECK_MINUTE,
    ARQ_DAILY_MONITOR_SUMMARY_REPORT_HOUR,
    ARQ_DAILY_MONITOR_SUMMARY_REPORT_MINUTE,
    ARQ_MONITOR_SUMMARY_REPORT_SPAN_HOURS,
    DEPLOY_ENV,
    GRAYLOG_HOST,
    GRAYLOG_PORT,
    LOG_TO,
    REDIS_DB_APS,
    REDIS_DB_ARQ,
    REDIS_HOST,
    REDIS_PORT,
    REDIS_PWD,
    cfg,
)
from fastapi import HTTPException
from monitors.models import MonitorBase
from monitors.utils import (
    format_email,
    format_rocketchat,
    get_monitor_by_id,
    get_monitors,
    update_cert_checks,
    update_monitor,
)
from notifiers.models import NotifierBase, NotifierKind, NotifierMessage
from notifiers.utils import get_notifier_by_id
from results.models import CertData, ResultBase, ResultExt, ResultStatus


def configure_logging() -> None:
    # NOTE: this is the logging config for arq logging.
    logging.config.dictConfig(
        {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "console": {
                    "class": "logging.Formatter",
                    "datefmt": "%H:%M:%S",
                    "format": "%(levelname)s:\t\b%(asctime)s %(name)s:%(lineno)d %(message)s",
                },
            },
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "formatter": "console",
                },
            },
            "loggers": {
                "arq": {
                    "handlers": ["console"],
                    "level": logging.getLevelName(APP_LOGLEVEL),
                    "propagate": True,
                },
                # NOTE: we often need the ability to set worker log level to WARNING while leaving app log level to DEBUG,
                #  to do so use the following static level for arq.
                # ~ 'arq': {'handlers': ['console'], 'level': 'WARNING', 'propagate': True},
            },
        }
    )


redis = None  # will contain the redis connection pool.
app_name = f"{cfg.core.name}-worker"

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(APP_LOGLEVEL))
graylog_handler = graypy.GELFUDPHandler(host=GRAYLOG_HOST, port=GRAYLOG_PORT)
console_handler = logging.StreamHandler()
if "graylog" in LOG_TO:
    logger_base.addHandler(graylog_handler)
if "console" in LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": app_name, "application_env": DEPLOY_ENV},
)

if APP_LOGLEVEL.upper() == "DEBUG":
    logger.info("### App Started With Debug On ###")

logger.info(
    f"{app_name} - v.{cfg.core.version} ({cfg.core.modified}) - {APP_LOGLEVEL} - {LOG_TO}"
)
logger.info(f"Scheduler DB: {REDIS_HOST}:{REDIS_PORT}/{REDIS_DB_APS}")
logger.info(f"Worker DB:    {REDIS_HOST}:{REDIS_PORT}/{REDIS_DB_ARQ}")
logger.info("Schedules")
logger.info(
    f"\t- set_cert_check: H={ARQ_DAILY_CERT_CHECK_HOUR}, M={ARQ_DAILY_CERT_CHECK_MINUTE}"
)
logger.info(
    f"\t- process_monitor_summary_report: H={ARQ_DAILY_MONITOR_SUMMARY_REPORT_HOUR}, M={ARQ_DAILY_MONITOR_SUMMARY_REPORT_MINUTE}"
)


def result_time_format(time):

    return round(time, 3) if time else None


async def send_notifications(
    monitor: MonitorBase = None,
    result: ResultBase = None,
):
    """
    Send notifications
    Loop through all monitor recipients and schedule notifications
    # @TODO: this should be merged with send_notifications; should be called notify_recipients and we need to abstract wrapping logic to handle send_notifications
    """
    for recipient in sorted(
        monitor.recipients, key=lambda recipient: recipient.notifier
    ):
        # get notifier
        _notifier = await get_notifier_by_id(recipient.notifier)
        if _notifier:
            notifier = NotifierBase(**_notifier)
            # enqueue the send_* job for notification delivery.
            nm = NotifierMessage()
            nm.notifier = notifier
            nm.destination = recipient.address
            nm.subject = f"{notifier.config.msg_subject} - {monitor.name}"
            if notifier.kind == NotifierKind.email:
                nm.body = format_email(
                    prefix=notifier.config.msg_prefix,
                    suffix=notifier.config.msg_suffix,
                    monitor=monitor,
                    result=result,
                )
            elif notifier.kind == NotifierKind.rocketchat:
                nm.body = format_rocketchat(
                    prefix=notifier.config.msg_prefix,
                    suffix=notifier.config.msg_suffix,
                    subject=notifier.config.msg_subject,
                    monitor=monitor,
                    result=result,
                )
            else:
                logger.error(
                    f"- unknown notifier kind for: {notifier.kind}, no sender job created!"
                )
                break
            await redis.enqueue_job(f"{notifier.kind.value}_sender", nm)
        else:
            logger.error(f"Notifier not found: {recipient.notifier}")
            break

    return True


async def email_sender(ctx, notifier_message: NotifierMessage):
    """
    Notify via email
    """

    logger.info("notify via email")

    server_url, server_port = notifier_message.notifier.config.server.split(":")

    import smtplib
    import ssl
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    msg = MIMEMultipart("alternative")
    msg["Subject"] = (
        f"{cfg.core.name} {notifier_message.subject}"
        if DEPLOY_ENV == "prd"
        else f"{cfg.core.name} {notifier_message.subject} ({DEPLOY_ENV})"
    )
    msg["From"] = notifier_message.notifier.config.username
    msg["To"] = notifier_message.destination
    # ~ part1 = MIMEText(f"{notifier_message.body}", "plain")
    msg_body = markdown.markdown(
        f"<style>table, th, td {{ border: 1px solid #7F7F7F; border-collapse: collapse; padding: 3 3 3 3; text-align: center;}} table {{ width: 70%; margin-left: 15%; margin-right: 15%; }} th {{ background-color: #7F7F7F; }} tr:nth-child(even) {{background-color: #f2f2f2;}}</style>{notifier_message.body}",
        extensions=["tables"],
        output_format="html5",
    )
    # ~ logger.critical(f"- email body formatted ({type(msg_body)}): {msg_body}")
    # ~ part1 = MIMEText(f"{markdowner.convert(notifier_message.body)}", "html")
    part1 = MIMEText(msg_body, "html")
    # part2 = MIMEText(text, 'html')
    msg.attach(part1)
    # msg.attach(part2)
    context = ssl.create_default_context()
    try:
        with smtplib.SMTP_SSL(server_url, server_port, context=context) as server:
            server.login(
                notifier_message.notifier.config.username,
                notifier_message.notifier.config.password,
            )
            server.send_message(msg)
            server.quit()
        return {"success": True, "message": "Message sent"}

    except Exception as e:
        msg = f"Error sending message: {e}"
        logger.error(msg)
        return {"success": False, "message": msg}

    return True


async def rocketchat_sender(ctx, notifier_message: NotifierMessage):
    """
    Notify via rocketchat
    """

    from requests import sessions
    from rocketchat_API.rocketchat import RocketChat

    logger.info("notify via rocketchat")
    # note: rocketchat has no 'subject' so we format the first line to act like a subject.
    subject = (
        f"{notifier_message.subject}"
        if DEPLOY_ENV == "prd"
        else f"{notifier_message.subject} ({DEPLOY_ENV})"
    )
    msg = f"_{subject}_\n{notifier_message.body}"
    try:
        with sessions.Session() as session:
            rocket = RocketChat(
                user_id=notifier_message.notifier.config.username,
                auth_token=notifier_message.notifier.config.password,
                server_url=notifier_message.notifier.config.server,
                session=session,
            )
            rocket.chat_post_message(
                msg,
                channel=notifier_message.destination,
                alias=notifier_message.notifier.config.msg_from,
            ).json()
        return {"success": True, "message": "Message sent"}
    except Exception as e:
        msg = f"Error sending message: {e}"
        logger.error(msg)
        return {"success": False, "message": msg}


async def notify_recipients(
    recipients: List[Recipient] = [], notifier_message: NotifierMessage = None
):
    """
    Notify recipients
    This function will send a message to all recipients.
    # @TODO: this should be merged with send_notifications; should be called notify_recipients and we need to abstract wrapping logic to handle send_notifications
    """

    logger.info("notify recipients")
    for recip in sorted(recipients, key=lambda recipient: recipient.notifier):
        _notifier = await get_notifier_by_id(recip.notifier)
        if _notifier:
            notifier = NotifierBase(**_notifier)
            notifier_message.notifier = notifier
            notifier_message.destination = recip.address
            await redis.enqueue_job(f"{notifier.kind.value}_sender", notifier_message)
        else:
            logger.error(f"Notifier not found: {recip.notifier}")

    return True


async def monitor(ctx, monitor_id):
    """
    Perform the monitor
    """

    from results.utils import add_result

    m_start = asyncio.get_event_loop().time()
    session: ClientSession = ctx["session"]
    logger.info(f"monitor id: {monitor_id}")
    monitor = await get_monitor_by_id(monitor_id=monitor_id)
    if not monitor["disabled"]:
        url = monitor["url"]
        timeout = monitor["timeout"]
        # note: we use cert_check_flag here as cert checks are only performed once per day.
        cert_check = monitor["cert_check_flag"]
        cert_expire_notify_limit = monitor["cert_expire_notify_limit"]
        expected_response_codes = monitor["expected_response_codes"]
        result = ResultExt()
        result.created = datetime.now()
        cert_data = CertData()
        result.status = ResultStatus.failure

        try:
            start = asyncio.get_event_loop().time()
            async with session.get(url, timeout=timeout, verify_ssl=False) as response:
                result.status_time = asyncio.get_event_loop().time() - start
                logger.info(f"{url} status: {response.status} in {result.status_time}")
                content = await response.text()
                logger.debug(f"{url} content: {content}")
                result.response_time = asyncio.get_event_loop().time() - start

            # create result.
            result.code = response.status
            result.url = str(response.url)
            result.meta_data = (
                response.headers if not cfg.api.monitors.results.compact_save else None
            )  # if compact_save we dont store meta_data.
            if result.code in expected_response_codes:
                result.status = ResultStatus.success
                logger.debug(f"{url} response code in expected range")
                # reset recent_fail_amount
                monitor["recent_fail_amount"] = 0
            else:
                result.status = ResultStatus.failure
                result.error = f"{url} response code [{result.status}] not in expected range: {expected_response_codes}"
                logger.info(f"{url} response code is NOT in expected range")
                # increment recent_fail_amount
                if monitor["recent_fail_amount"]:
                    monitor["recent_fail_amount"] += 1
                else:
                    monitor["recent_fail_amount"] = 1

            logger.debug(
                f"{url} cert check test: {cert_check}/{result.status} : true/{ResultStatus.success}"
            )

            # NOTICE: if we have a failure in monitor we do not check the cert.
            if cert_check and result.status == ResultStatus.success:
                logger.debug("{url} performing cert check")
                (cert_data.valid, cert_data.expire_date, cert_data.past_notify) = check(
                    url, cert_expire_notify_limit
                )
                logger.info(f"{url} cert valid? {cert_data.valid}")
                # if cert is valid and not past notify date then result is success, otherwise it is a failure.
                result.status = (
                    ResultStatus.success
                    if cert_data.valid and not cert_data.past_notify
                    else ResultStatus.failure
                )
                result.cert_data = cert_data
                # Reset cert_check_flag so cert check is not performed again (note: cron job will reset it once per day).
                monitor["cert_check_flag"] = False

            # update monitor with recent_fail_amount and/or cert_data.
            _mon_update = await update_monitor(doc=monitor)
            # ~ logger.debug(f"- monitor update results: {_mon_update}")
            if _mon_update["ok"] == "fail":
                logger.critical("Failed to update monitor: {_mon_update}")
                # @TODO: we should notify or log this somewhere!

            logger.debug(f"{url} result is: {result}")

        except ClientConnectorError as e:
            logger.error(
                f"Client Connector Error (url doesn't exist?) for monitor={monitor_id}, url={url}\nMessage: {e}"
            )
            result.error = str(e)
        except (asyncio.CancelledError) as e:
            logger.error(
                f"Cancelled Error for monitor={monitor_id}, url={url}\nMessage: {e}"
            )
            result.error = f"Cancelled Error for monitor: {e}"
        except (asyncio.TimeoutError) as e:
            logger.error(
                f"Timeout Error for monitor={monitor_id}, url={url}\nMessage: {e}"
            )
            result.error = f"Timeout Error for monitor: {e}"
        except Exception as e:
            logger.error(
                f"Other Error occurred for monitor={monitor_id}, url={url}\nMessage: {e}"
            )
            result.error = str(e)

        # save the result if result is successful and success_save = true
        if (
            result.status == ResultStatus.success
            and not cfg.api.monitors.results.success_save
        ):
            logger.info(
                f"{url} successful monitor result not saved due to api.monitors.results.success_save settings"
            )
        else:
            result.monitor_time = asyncio.get_event_loop().time() - m_start
            # @TODO seems we should add generic logic where any error/warnings are sent to RC?
            try:
                logger.debug(f"updating result for {url} with: {result.dict()}")
                _result = await add_result(monitor_id=monitor_id, doc=result)
                logger.debug(f"- insert result results: {_result}")
            except Exception as e:
                raise HTTPException(status_code=409, detail=f"Insert exception: '{e}'")
            # if result.status is failure then send notifications.
            if result.status == ResultStatus.failure:
                logger.info(f"{url} monitor found issue, send notifications")
                # ensure recent_fail_amount is greater than recent_fail_notify_amount before notifying.
                if (
                    monitor["recent_fail_amount"]
                    >= monitor["recent_fail_notify_amount"]
                ):
                    logger.info(
                        f"{url} recent_fail_amount [{monitor['recent_fail_amount']}] >= recent_fail_notify_amount [{monitor['recent_fail_notify_amount']}], sending notification"
                    )
                    r = await send_notifications(MonitorBase(**monitor), result)
                    logger.info(f"{url} notifications sent with result: {r}")
        logger.debug(f"{url} monitor time: {result.monitor_time}")

        return result.status
    else:
        return {"success": False, "message": f"monitor disabled for {monitor['url']}"}


async def set_cert_check(ctx):
    """
    Schedule cert checks
    """

    # ~ logger.info("update all monitors with cert_check = True")
    await update_cert_checks()
    # ~ logger.info("all monitors with cert_check = True updated!")


async def monitor_summary_report(ctx, account_name):
    """
    Handle the monitor summary report for an Account (name).

    NOTE: account will be skipped if it is disabled or summary_report is not enabled
    NOTE: process one account at a time, each account can/will have multiple monitors
    NOTE: monitors will be skipped if it is disabled or summary_report is not enabled
    """

    from results.utils import monitor_results_for_span

    report_span_hours = ARQ_MONITOR_SUMMARY_REPORT_SPAN_HOURS
    to_date = arrow.utcnow()
    from_date = to_date.shift(hours=report_span_hours)
    account = await get_account(account_name)
    logger.debug(f"- running MSR on account: {account} from: {from_date} to: {to_date}")
    # check to ensure account is not disabled and has summary_report enabled
    if account.disabled or not account.summary_report:
        _msg = f"- Summary Reports skipped for {account.name}\n  - disabled: {account.disabled}, summary_report: {account.summary_report}"
        logger.warning(_msg)
        return DocInfo(
            ok="no",
            msg=_msg,
        )

    logger.info(
        f"report metadata:\n  span hours: {report_span_hours}\n  start date: {from_date}"
    )

    account_id = account.id_
    logger.info(f"processing summary report for: {account} (account_id={account_id})")

    # ~ msg_list = ["Monitor Summary Report"]
    msg_list = [
        ""
    ]  # note: need a blank line before the 'table' so RC sees it as a table and formats correctly.

    # write header row
    msg_list.append("Monitor | Volume | Success | Failure | ART | Cert Exp ")
    msg_list.append("-- | -- | -- | -- | -- | --")

    # get all enabled monitors with summary info for users account.
    logger.info(f"For account: {account_id}")
    for monitor in await get_monitors(
        account=account_id, disabled=False, summary_report=True
    ):
        # get all results for the monitor over the given timespan
        volume = 0
        success = 0
        failure = 0
        response_time_sum = 0
        cert_exp_date = None

        _results = await monitor_results_for_span(
            monitor_id=monitor["_id"],
            start=from_date,
            end=to_date,
        )

        for result in _results:
            volume += 1
            success += 1 if result.status == ResultStatus.success else 0
            failure += 1 if result.status == ResultStatus.failure else 0
            response_time_sum += (
                result.response_time if result and result.response_time else 0
            )
            if result.cert_data:
                cert_exp_date = arrow.get(result.cert_data.expire_date).format(
                    "YYYY-MM-DD"
                )

        if volume > 0:
            ssl_cert_exp = f"{cert_exp_date} (UTC)" if cert_exp_date else "n/a"
            msg_list.append(
                f"[{monitor['name']}]({monitor['url']}) | {volume} | {success} ({round((success/volume)*100, 2)}%) | {failure} ({round((failure/volume)*100, 2)}%) | {round(response_time_sum/volume, 4)} | {ssl_cert_exp}"
            )
        else:
            msg_list.append(f"{monitor['name']} | - | - | - | ERROR: no volume")

    # blank line needed to separate from table.
    msg_list.append("")
    msg_list.append("&nbsp;")
    msg_list.append("")
    msg_list.append("&nbsp;")

    # get all disabled monitors for users account.
    logger.info(f"For account: {account_id}")

    disabled_monitors = await get_monitors(account=account_id, disabled=True)
    msg_list.append(
        f"NOTE: You have {len(disabled_monitors)} disabled Monitors: {', '.join([x['name'] for x in disabled_monitors])}"
    )

    msg_list.append("")
    msg_list.append("&nbsp;")
    msg_list.append(
        f"data from: **{from_date.to('local').format('YYYY-MM-DD HH:mm')}** to **{arrow.utcnow().to('local').format('YYYY-MM-DD HH:mm')}**"
    )

    nm = NotifierMessage()
    nm.subject = f"MSR for: {account.name}"
    nm.body = "\n".join(msg_list)
    nr = await notify_recipients(recipients=account.recipients, notifier_message=nm)

    return nr


async def process_monitor_summary_report(ctx):
    """
    Process the monitor summary report
    """

    from accounts.utils import get_accounts

    logger.info("processing monitor summary report")
    # NOTICE: this process uses the task queue itself to handle running
    #  the report for all accounts. A job is created for each account.

    # get all accounts with summary_report enabled that are not disabled.
    for account in await get_accounts(disabled=False, summary_report=True):
        logger.info(
            f"enqueue monitor summary report task for account: {account['name']}"
        )
        # enqueue job so worker(s) can perform the summary report.
        job = await redis.enqueue_job("monitor_summary_report", account["name"])

    logger.info(f"monitor summary report enqueued with job id: {job.job_id}")


async def monitor_results_report(ctx, monitor_id, monitor_name, account, start, end):
    """
    Handle the monitor results report for a monitor
    """

    from results.utils import monitor_results_for_span

    logger.info(
        f"processing monitor results report for: {monitor_id} ({monitor_name}) with span: {start} :to: {end}"
    )

    # ~ msg_list = ["Monitor Results Report"]
    msg_list = [
        ""
    ]  # note: need a blank line before the 'table' so RC sees it as a table and formats correctly.

    # write header row
    msg_list.append(
        "Date/Time | Status | Code | Status Time | Response Time | Monitor Time | Error | Valid | Expires | Past Notify "
    )
    msg_list.append(
        " -------- | ------ | ---- | ----------- | ------------- | ------------ | ----- | ----- | ------- | ----------- "
    )

    logger.debug("- getting monitor results for span...")
    _sta = []
    _res = []
    _mon = []
    _results = await monitor_results_for_span(
        monitor_id=monitor_id,
        start=start,
        end=end,
    )
    logger.debug(
        f"- got monitor results for span ({len(_results)}), format and return..."
    )

    # sort results by created (datetime)
    # ~ for result in sorted(_results, key=itemgetter(*["created"])):
    for result in _results:
        # logger.info(f"datetime: {result['created']}, {result['status']}, {result['code']}, {result['status_time'] if 'status_time' in result else ''}, {result['cert_data']}")
        # ~ r = ResultExt(**result)
        r = result
        r.error = "" if not r.error else f"<pre>{r.error}</pre>"
        msg_list.append(
            f"{arrow.get(r.created).format('YYYY-MM-DD HH:mm')} | {r.status} | {r.code} | {result_time_format(r.status_time)} | {result_time_format(r.response_time)} | {result_time_format(r.monitor_time)} | {r.error} | {r.cert_data.valid if r.cert_data else ''} | {r.cert_data.expire_date if r.cert_data else ''} | {r.cert_data.past_notify if r.cert_data else ''}"
        )
        (_sta.append(r.status_time) if r.status_time else None)
        (_res.append(r.response_time) if r.response_time else None)
        (_mon.append(r.monitor_time) if r.monitor_time else None)

    msg_list.append(" | | | | | | | | | ")
    msg_list.append(
        f" | | **Count** | **{len(_sta)}** | **{len(_res)}** | **{len(_mon)}** | "
    )
    msg_list.append(
        f" | | **Avg** | **{result_time_format(sum(_sta)/len(_sta)) if len(_sta) > 0 else None}** | **{result_time_format(sum(_res)/len(_res)) if len(_res) > 0 else None}** | **{result_time_format(sum(_mon)/len(_mon)) if len(_mon) > 0 else None}** | "
    )
    msg_list.append(
        f" | | **Min** | **{result_time_format(min(_sta)) if len(_sta) > 0 else None}** | **{result_time_format(min(_res)) if len(_res) > 0 else None}** | **{result_time_format(min(_mon)) if len(_mon) > 0 else None}** | "
    )
    msg_list.append(
        f" | | **Max** | **{result_time_format(max(_sta)) if len(_sta) > 0 else None}** | **{result_time_format(max(_res)) if len(_res) > 0 else None}** | **{result_time_format(max(_mon)) if len(_mon) > 0 else None}** | "
    )

    # blank line needed to separate from table.
    msg_list.append("")
    msg_list.append("&nbsp;")
    msg_list.append("")
    msg_list.append("&nbsp;")
    msg_list.append(f"data from: **{start}** to **{end}**")

    nm = NotifierMessage()
    nm.subject = f"MRR for: {monitor_name}"
    nm.body = "\n".join(msg_list)
    nr = await notify_recipients(recipients=account.recipients, notifier_message=nm)

    return nr


async def startup(ctx):
    global redis

    configure_logging()
    ctx["session"] = ClientSession()

    redis = await create_pool(
        RedisSettings(
            host=REDIS_HOST, port=REDIS_PORT, database=REDIS_DB_ARQ, password=REDIS_PWD
        )
    )


async def shutdown(ctx):
    await ctx["session"].close()


# WorkerSettings defines the settings to use when creating the work,
# it's used by the arq cli
class WorkerSettings:
    # NOTE: container runs with UTC time.
    cron_jobs = [
        cron(
            set_cert_check,
            hour=ARQ_DAILY_CERT_CHECK_HOUR,
            minute=ARQ_DAILY_CERT_CHECK_MINUTE,
        ),
        # NOTICE: the msr should run a few minutes after the cert_check to ensure cert data has been pulled
        cron(
            process_monitor_summary_report,
            # hour=None,
            # minute={0,2,4,6,8,10,12,14,16,18,20,24,26,28,30},
            # minute=41,
            # second=33,
            hour=ARQ_DAILY_MONITOR_SUMMARY_REPORT_HOUR,
            minute=ARQ_DAILY_MONITOR_SUMMARY_REPORT_MINUTE,
        ),
    ]
    functions = [
        monitor,
        monitor_summary_report,
        monitor_results_report,
        email_sender,
        rocketchat_sender,
    ]
    on_startup = startup
    on_shutdown = shutdown
    health_check_interval = 60

    redis_settings = RedisSettings(
        host=REDIS_HOST,
        port=REDIS_PORT,
        database=REDIS_DB_ARQ,
        password=REDIS_PWD,
    )

    # job_timeout default: 300 (5m)
    job_timeout = 1200  # 20m


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
