#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging

import aiocouch
import arrow
from _common.utils import fix_id
from accounts.utils import get_account
from config.config import couchdb
from fastapi import HTTPException
from monitors.models import _db

logger = logging.getLogger("default")


def format_email(
    prefix=None, suffix=None, monitor=None, result=None, message: str = None
):
    """Format an email message"""

    # add notification time.
    r = f"At: {arrow.get(result.created).to('America/New_York').format('YYYY-MM-DD HH:mm')} | Fails: {monitor.recent_fail_amount}\n\n"

    # if result has cert_data it is a cert_check "type", otherwise it is a normal result.
    if not result.cert_data:
        # Format: normal
        r = f"{r}{result.url} threw a /{result.code}/ - {result.error}"
    else:
        # Format: cert check
        expire_date = arrow.get(result.cert_data.expire_date)
        r = f"""{r}{monitor.url} failed cert checks.
- cert valid:   {result.cert_data.valid}
- cert expires: {expire_date.format('YYYY-MM-DD')} ({expire_date.humanize(granularity='day')})
- past_notify:  {result.cert_data.past_notify} (limit: {monitor.cert_expire_notify_limit} days)"""

    # prepend/append needed items.
    r = f"{prefix}\n{r}" if prefix else r
    r = f"- message: {message}" if message else r
    r = f"{r}\n{suffix}" if suffix else r

    return r


def format_rocketchat(
    prefix=None,
    suffix=None,
    subject=None,
    monitor=None,
    result=None,
    message: str = None,
):
    """Format a rocketchat message"""

    # add notification time.
    r = f"At: {arrow.get(result.created).to('America/New_York').format('YYYY-MM-DD HH:mm')} | Fails: {monitor.recent_fail_amount}\n\n"

    # if result has cert_data it is a cert_check "type", otherwise it is a normal result.
    if not result.cert_data:
        # Format: normal
        r = f"{r}{result.url} threw a `{result.code}` - _{result.error}_"
    else:
        # Format: cert check
        expire_date = arrow.get(result.cert_data.expire_date)
        r = f"""{r}{monitor.url} failed cert checks.
- cert valid: `{result.cert_data.valid}`
- cert expires: `{expire_date.format('YYYY-MM-DD')}` ({expire_date.humanize(granularity='day')})
- past_notify:  `{result.cert_data.past_notify}` (limit={monitor.cert_expire_notify_limit}days)"""

    # prepend/append needed items.
    r = f"{prefix}\n{r}" if prefix else r
    r = f"\n{message}" if message else r
    r = f"{r}\n{suffix}" if suffix else r

    return r


async def get_monitor_by_id(monitor_id: str = None):
    """
    Get Monitor by id
    @return: document|None
    @unsafe: this is an unsafe get_monitor as it does not check access!
    """

    logger.debug(f"get monitor by id: {monitor_id}")
    db = await couchdb[_db.value]
    try:
        return fix_id(await db[monitor_id])
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


async def get_monitor(
    name: str = None, account: str = None, check_account_access: bool = True
):
    """
    Get Monitor by name and account.
    @return: document|None
    @unsafe: this is an unsafe get_monitor as it does not check access!
    """

    logger.debug(
        f"- get monitor for name={name}, account={account}, check_account_access={check_account_access}"
    )
    if check_account_access:
        pass  # not yet implemented
        # check to ensure user has access for account.
        # ~ if not await has_account_access(current_user, account, roles=role):
        # ~ raise HTTPException(status_code=401, detail="Not authorized")

    _account = await get_account(account)
    logger.debug(f"  - found account: {_account}")
    if not _account:
        logger.error("ERROR: Account not found")
        return None

    db = await couchdb[_db.value]
    selector = {"name": name, "account": _account.id_}

    docs = []
    async for doc in db.find(selector=selector):
        docs.append(fix_id(doc))

    if len(docs) == 1:
        return docs[0]
    else:
        # ~ logger.debug(f"- found docs: {len(docs)}:\n{docs}")
        logger.error(
            f"ERROR: Unexpected Number of Results! Error in getting monitor by name, too many matches were found, expecting 1 found [{len(docs)}]: {docs}"
        )
        return None


async def get_monitors(
    account: str = None, disabled: bool = None, summary_report: bool = None
):
    """
    Get monitors for an Account.
    if additional kwargs are supplied they are applied to the filter to get monitors.
    """

    db = await couchdb[_db.value]
    # _account = await get_account(account)
    # logger.debug(f"- account ({account}) lookup: {_account}")
    selector = {"account": account}
    if disabled is not None:
        selector["disabled"] = disabled
    if summary_report is not None:
        selector["summary_report"] = summary_report
    logger.debug(f"- selector: {selector}")

    docs = []
    async for doc in db.find(selector=selector):
        docs.append(fix_id(doc))

    return docs


async def update_cert_checks():
    """
    Update all enabled monitors to set cert checks 'on'
    """

    logger.info("Update all enabled monitors with cert_check = True to check...")
    db = await couchdb[_db.value]
    selector = {"disabled": False, "cert_check": True}

    async for doc in db.find(selector=selector):
        logger.debug(
            f"- update cert checks on id={doc['id_']} - {doc['account']}/{doc['name']}"
        )
        doc["cert_check_flag"] = True
        await doc.save()

    ret_msg = "- all monitors with cert_check = True updated!"
    logger.info(ret_msg)
    return {"success": True, "message": ret_msg}


async def update_monitor(
    doc=None,  # the monitor document
):
    """
    Update monitor
    NOTICE: UNSAFE, no access checks are performed here!
    """

    from monitors.models import Monitor
    from monitors.routes import _scheduler_add

    # ~ logger.debug(f"- updating monitor with doc: {doc}")
    try:
        await doc.save()
        logger.debug("add scheduler job (removing old first)")
        # NOTE: _scheduler_add first deletes the existing job and then adds as new.
        # NOTE: if monitor has been disabled the _scheduler_add will not add a new one after the delete which in essence removes the scheduler for the monitor
        ms = await _scheduler_add(monitor=Monitor(**doc))
        logger.debug(f"- scheduler_add response: {ms}")
        if ms["success"]:
            logger.debug("- scheduler add success...")
            return {
                "ok": "ok",
                "id": doc["_id"],
                "msg": f"Monitor updated and {ms['message']}",
            }
        else:
            logger.info(
                "Could not create scheduler job (this is normal in the case of a disabled monitor)"
            )
            return {"ok": "fail", "id": doc["_id"], "msg": "Could not create scheduler"}

    except aiocouch.ConflictError as e:
        return {"ok": "fail", "id": doc["_id"], "msg": f"Issue saving doc: {e}"}
