#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

from datetime import datetime
from typing import List, Optional, Union

from _common.models import Recipient, RunInterval
from config.models import Cdb
from pydantic import BaseModel, HttpUrl

_db = Cdb.MONITORS


class MonitorBase(BaseModel):
    """
    Base
    """

    name: str = None
    account: str = None  # id of the account to which the notifier belongs
    url: HttpUrl = None
    run_interval: RunInterval = None
    timeout: int = 20
    summary_report: bool = None  # is summary report (MSR) enable?
    recent_fail_notify_amount: int = 1  # send notification when we reach this amount
    recent_fail_amount: Optional[int] = None

    cert_check: bool = False
    cert_check_flag: bool = (
        False  # flag set by cron job which controls if cert check will be ran
    )
    cert_expire_notify_limit: Optional[
        int
    ] = 30  # number of days before cert expires to notify.

    recipients: List[Recipient] = []
    expected_response_codes: Union[List[int], str] = []

    note: Optional[str] = None
    disabled: bool = False  # is account disabled?


class MonitorExt(MonitorBase):
    """
    Extended (added by backend logic)
    """

    created: datetime = datetime.now()


class Monitor(MonitorExt):
    """
    Actual (at DB level)
    """

    id_: str
