#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import inspect
import logging
from typing import List

import aiocouch
import requests
from _common.models import DocInfo, Recipient, RunInterval
from _common.utils import codes_to_list, fix_id, generate_id, get_route_roles, make_id
from accounts.models import Account
from accounts.utils import get_account
from auth.utils import has_account_access
from config.config import SCHEDULER_ENDPOINT_URL, cfg, couchdb
from fastapi import APIRouter, HTTPException, Query, Security, status
from fastapi.encoders import jsonable_encoder
from monitors.models import Monitor, MonitorBase, _db
from monitors.utils import get_monitor, get_monitor_by_id, get_monitors, update_monitor
from pydantic import HttpUrl
from users.models import UserBase, get_current_user

logger = logging.getLogger("default")
monitors_router = APIRouter()


async def _scheduler_delete(monitor: Monitor = None):
    """
    Delete a scheduler job
    """

    # delete (if exists) any job before adding a new one.
    headers = {
        "User-Agent": f"{cfg.core.name} {cfg.core.version}",
    }
    r = requests.delete(
        f"{SCHEDULER_ENDPOINT_URL}/monitor_{monitor.id_}", headers=headers
    )
    return {"success": r.ok, "message": r.text, "code": r.status_code}


async def _scheduler_add(monitor: Monitor = None):
    """
    Schedule monitor
    NOTE: current scheduler job is removed before being re-added
    """

    logger.debug("add scheduler job, removing current...")
    sd = await _scheduler_delete(monitor=monitor)
    if not sd["success"]:
        logger.warning(f"Problem removing old scheduler: {sd}")
    else:
        logger.debug("old scheduler job removed")

    if monitor.disabled:
        return {"success": True, "message": "monitor disabled, cannot schedule."}
    else:
        headers = {
            "User-Agent": f"{cfg.core.name} {cfg.core.version}",
        }
        params = {
            # 'url':monitor['url'],
            # 'timeout': monitor['timeout'],
            "monitor_id": monitor.id_,
            # # "run_interval": monitor["run_interval"]["seconds"],
            # 'cert_check': monitor["cert_check_flag"] if "cert_check_flag" in monitor else False,
            # 'cert_expire_notify_limit': monitor["cert_expire_notify_limit"] if "cert_expire_notify_limit" in monitor else None,
            # 'expected_response_codes': monitor["expected_response_codes"],
        }
        r = requests.post(
            f"{SCHEDULER_ENDPOINT_URL}/schedule-monitor",
            headers=headers,
            params=params,
            json=jsonable_encoder(monitor.run_interval),
        )
        if r.ok:
            logger.debug(f"request scheduled with response ({r.status_code}): {r.text}")
            return {
                "success": True,
                "message": f"monitor scheduled with id: monitor_{monitor.id_}",
            }
        else:
            msg = f"Unable to schedule monitor: ({r.status_code}):\n{r.text}"
            logger.error(msg)
            return {"success": False, "message": msg}


async def _get_or_404(_id):
    """
    Get document by id or raise a 404
    """

    # ~ logger.debug(f"_get_or_404 request on: {_id.lower()}")
    db = await couchdb[_db.value]
    try:
        doc = fix_id(await db[_id.lower()])
        return doc
    except aiocouch.exception.NotFoundError:
        raise HTTPException(status_code=404, detail="Not found")


@monitors_router.get("/", response_model=List[Monitor])
async def get_all(
    account: str = Query(..., description="account in question"),
    disabled: bool = Query(None, description="filter on disabled status"),
    limit: int = 10,
    skip: int = 0,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Get all monitors for an account
    """

    db = await couchdb[_db.value]
    _account = await get_account(account)
    logger.debug(f"- account ({account}) lookup: {_account}")
    selector = {"account": _account.id_}
    if disabled is not None:
        selector["disabled"] = disabled

    docs = []
    async for doc in db.find(selector=selector, limit=limit, skip=skip):
        docs.append(fix_id(doc))

    return docs


@monitors_router.get("/{id}", response_model=Monitor)
async def get_by_id(
    _id: str = Query(..., description="The id of the document to get"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get by id
    """

    return await _get_or_404(_id)


@monitors_router.get("/by_name/", response_model=Monitor)
async def get_by_name(
    account: str = Query(..., description="account in question"),
    name: str = Query(..., description="name to get"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Get by Name and Account
    """

    logger.debug("- monitor get_by_name for name={name}, account={account}")
    return await get_monitor(name=name, account=account, check_account_access=True)


@monitors_router.post("/", response_model=DocInfo, status_code=status.HTTP_201_CREATED)
async def add(
    obj: MonitorBase,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Add new
    """

    # check to ensure user has access for account.
    acc = await get_account(obj.account)
    _rrole = get_route_roles(inspect.currentframe())
    if not await has_account_access(current_user, account=acc, roles=_rrole):
        raise HTTPException(status_code=401, detail="Not authorized")

    logger.debug("- must have account access...")

    _doc = MonitorBase(
        name=obj.name,
        account=acc.id_,
        url=obj.url,
        timeout=obj.timeout,
        summary_report=obj.summary_report,
        recent_fail_notify_amount=obj.recent_fail_notify_amount,
        cert_check=obj.cert_check,
        cert_expire_notify_limit=obj.cert_expire_notify_limit,
        run_interval=obj.run_interval,
        recipients=obj.recipients,
        expected_response_codes=codes_to_list(obj.expected_response_codes),
        note=obj.note,
        disabled=obj.disabled,
    )

    db = await couchdb[_db.value]

    try:
        doc_id = make_id(items=[generate_id()])
        doc = await db.create(
            doc_id,
            data=jsonable_encoder(_doc),
        )
        await doc.save()
        _monitor = await get_monitor_by_id(monitor_id=doc_id)
        _result = await _scheduler_add(monitor=Monitor(**_monitor))
        if not _result["success"]:
            logger.error("Problem creating scheduler for monitor")
            raise HTTPException(status_code=428, detail="Could not create scheduler")
        else:
            return await doc.info()

    except aiocouch.ConflictError:
        logger.error(f"Duplicate key: cannot add '{doc_id}'")
        return None


@monitors_router.put("/", response_model=DocInfo)
async def update(
    account: str = Query(..., description="account in question"),
    name: str = Query(..., description="name to get"),
    url: HttpUrl = Query(None, description="url of the site to monitor"),
    timeout: int = Query(
        None,
        description="max amount of time (in seconds) to wait for successful response",
    ),
    summary_report: bool = Query(None, description="Enabled summary report"),
    expected_response_codes: str = Query(
        None,
        description="expected response code(s) - e.g. '200', '200, 201, 203', '200-300, 303'",
    ),
    recent_fail_notify_amount: int = Query(
        None,
        description="trigger notifications when this amount of failures have been reached",
    ),
    cert_check: bool = Query(None, description="check ssl certs for url?"),
    cert_check_flag: bool = Query(
        None,
        description="flag set by cron job which controls if cert check will be ran",
    ),
    cert_expire_notify_limit: int = Query(
        None, description="number of days before ssl cert expires to notify on"
    ),
    run_interval: RunInterval = None,
    recipients: List[Recipient] = None,
    disabled: bool = Query(None, description="monitor disabled"),
    note: str = Query(
        None, description="Note about monitor (set to '.' to clear the note field)"
    ),
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Update monitor
    NOTICE: you cannot update the Name or Account
    """

    doc = await get_monitor(name=name, account=account, check_account_access=True)
    logger.debug(f"- updating monitor: {doc}")

    _check_account = await get_account(account) if account else None
    # use account from monitor if there is no account specified.
    if not account:
        _check_account = Account(**{"id_": doc["account"]})
    # check to ensure use has access for account.
    # ~ acc = await get_account(account)
    logger.debug(f"- checking with account: {_check_account}")
    if not await has_account_access(
        current_user,
        account=_check_account,
        roles=get_route_roles(inspect.currentframe()),
    ):
        raise HTTPException(status_code=401, detail="Not authorized")

    # ~ doc["name"] = name if name else doc["name"]
    # ~ doc["account"] = account if account else doc["account"]
    doc["url"] = url if url else doc["url"]
    doc["timeout"] = timeout if timeout else doc["timeout"]
    doc["summary_report"] = summary_report if summary_report else doc["summary_report"]
    doc["cert_check"] = cert_check if cert_check else doc["cert_check"]
    doc["cert_check_flag"] = (
        cert_check_flag if cert_check_flag else doc["cert_check_flag"]
    )
    doc["cert_expire_notify_limit"] = (
        cert_expire_notify_limit
        if cert_expire_notify_limit
        else doc["cert_expire_notify_limit"]
    )
    doc["disabled"] = disabled if disabled is not None else doc["disabled"]
    doc["run_interval"] = (
        jsonable_encoder(run_interval) if run_interval else doc["run_interval"]
    )
    doc["recipients"] = (
        jsonable_encoder(recipients) if recipients else doc["recipients"]
    )
    doc["expected_response_codes"] = (
        codes_to_list(expected_response_codes)
        if expected_response_codes
        else doc["expected_response_codes"]
    )
    if note:
        doc["note"] = "" if note == "." else note
    else:
        doc["note"]

    _mon_update = await update_monitor(doc=doc)
    if _mon_update["ok"] == "ok":
        return _mon_update
    else:
        raise HTTPException(status_code=409, detail=_mon_update["msg"])


@monitors_router.delete("/{id}", response_model=dict)
async def delete(
    _id: str,
    current_user: UserBase = Security(get_current_user, scopes=["users"]),
):
    """
    Delete monitor
    """

    _monitor_doc = await get_monitor_by_id(_id)
    if not _monitor_doc:
        raise HTTPException(status_code=404, detail="Monitor not found")

    _monitor = Monitor(**_monitor_doc)
    logger.debug(f"- deleting monitor: {_monitor}")

    # check to ensure use has access for account.
    _account = Account(**{"id_": _monitor.account})

    logger.debug("- checking account access...")
    if not await has_account_access(
        current_user, account=_account, roles=get_route_roles(inspect.currentframe())
    ):
        raise HTTPException(status_code=401, detail="Not authorized")

    logger.debug("- deleting scheduler...")

    # delete scheduler job
    _scheduler_delete_res = await _scheduler_delete(monitor=_monitor)
    if not _scheduler_delete_res["success"]:
        logger.warning(f"Problem removing old scheduler: {_scheduler_delete_res}")

    # delete the monitor (even if job delete fails)
    logger.debug("- deleting monitor...")
    resp = DocInfo(ok="ok", id=_monitor.id_, rev="", msg="deleted")
    _delete_res = await _monitor_doc.delete(discard_changes=True)
    # note: _delete_res will be None if delete succeeds.
    if _delete_res:
        resp.ok = "fail"
        resp.msg = _delete_res

    return resp


@monitors_router.get("/set_status/", response_model=DocInfo)
async def set_status(
    account: str = Query(..., description="account in question"),
    name: str = Query(
        ..., description="name|list of names|all of monitor to enable/disable"
    ),
    disabled: bool = Query(False, description="disable the monitor(s)"),
    current_user: UserBase = Security(get_current_user, scopes=["admins"]),
):
    """
    Set the Enabled or Disable status of a monitor, set of monitors, or all monitors:
    - if a single name is provided (e.g. 'dradux') that monitor will be enabled/disabled.
    - if a csv list of names is provided (e.g. 'dradux,ukse,radiant') those monitors will be enabled/disabled
    - if 'all' is provided all monitors will be enabled/disabled
    """

    logger.debug(f"- set disabled={disabled} for account={account} and monitors={name}")
    _account = await get_account(account)
    logger.debug(f"- found account: {_account}")
    if name == "all":
        for monitor in await get_monitors(account=_account.id_):
            monitor["disabled"] = (
                disabled if disabled is not None else monitor["disabled"]
            )
            await update_monitor(doc=monitor)
    elif name.__contains__(","):
        # if name contains , split it as we have a list of monitor names
        monitor_names = name.split(",")
        for name in monitor_names:
            monitor = await get_monitor(
                name=name, account=account, check_account_access=False
            )
            monitor["disabled"] = (
                disabled if disabled is not None else monitor["disabled"]
            )
            await update_monitor(doc=monitor)
    else:
        # we have a single monitor
        monitor = await get_monitor(
            name=name, account=account, check_account_access=False
        )
        monitor["disabled"] = disabled if disabled is not None else monitor["disabled"]
        await update_monitor(doc=monitor)

    return DocInfo(
        ok="ok",
        id=None,
        rev=None,
        msg=f"monitor ({name}) status set to {'disabled' if monitor['disabled'] else 'enabled'}",
    )
