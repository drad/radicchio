#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

from datetime import date
from enum import Enum
from typing import Optional

from pydantic import BaseModel


class Cdb(Enum):
    """
    Available databases
    """

    ACCOUNTS = "accounts"
    MONITORS = "monitors"
    NOTIFIERS = "notifiers"
    RESULTS = "results"

    # ACTIVITIES = "activities"
    # ACTIVITY_MEDIA = "activity_media"
    # LOGS = "logs"
    # RELATED_ITEM_OVERVIEWS = "related_item_overviews"
    # RELATED_ITEM_TYPES = "related_item_types"
    USERS = "users"
    # USER_MESSAGES = "user_messages"


class ConfigFastApi(BaseModel):
    """
    FastAPI specific config.
    """

    debug: bool = False


class ConfigApiVersions(BaseModel):
    """
    API Versions.
    """

    current: str = None
    supported: str = None
    deprecated: str = None
    unsupported: str = None


class ConfigApiJwt(BaseModel):
    """
    API JWT config.
    """

    secret_key: str = None
    algorithm: str = None


class ConfigRouteAuthItem(BaseModel):
    """
    Route Auth Item.
    """

    get_all: Optional[list] = None
    get_one: Optional[list] = None
    add: Optional[list] = None
    update: Optional[list] = None
    delete: Optional[list] = None


class ConfigRouteAuth(BaseModel):
    """
    Route Auth config.
    """

    activities: ConfigRouteAuthItem = None
    related_item_overviews: ConfigRouteAuthItem = None
    related_item_types: ConfigRouteAuthItem = None


class ConfigApiMonitorsResults(BaseModel):
    """
    API Monitors Results
    """

    compact_save: bool = True
    success_save: bool = True


class ConfigApiMonitors(BaseModel):
    """
    API Monitors
    """

    results: ConfigApiMonitorsResults = None


class ConfigApi(BaseModel):
    """
    API specific config.
    """

    versions: ConfigApiVersions = None
    jwt: ConfigApiJwt = None
    route_auth: dict = None
    monitors: ConfigApiMonitors = None


class ConfigCore(BaseModel):
    """
    Core specific config.
    """

    name: str = None
    description: str = None
    version: str = None
    created: date = None
    modified: date = None


class ConfigBase(BaseModel):
    """
    Config Base.
    """

    fastapi: ConfigFastApi = None
    api: ConfigApi = None
    core: ConfigCore = None
    # database:
