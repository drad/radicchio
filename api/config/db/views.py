# Application Database Configuration for: views
"""
This file contains all views for the application and is ran upon application
start (via check_databases() on fastapi 'startup' event).

Structure:
  - get current view contents with {db}/_design/{view}
  - add an entry in the list below as:
    views.append({"database": "{db}", "views": [{ <contents from get response> }, {etc...}]})
"""

views = []

#
# Create ONE instance per database which can have one or more design docs which
#   can have one or more views.
#

# ~ views.append(
# ~ {
# ~ "database": "results",
# ~ "views": [
# ~ {
# ~ "_id": "_design/idx-created",
# ~ "_rev": "1-c8efce447867257550b5b3edf27be8ed",
# ~ "language": "query",
# ~ "options": {
# ~ "partitioned": True
# ~ },
# ~ "views": {
# ~ "created-json-index": {
# ~ "map": {
# ~ "fields": {
# ~ "created": "desc"
# ~ },
# ~ "partial_filter_selector": {}
# ~ },
# ~ "options": {
# ~ "def": {
# ~ "fields": [
# ~ {
# ~ "created": "desc"
# ~ }
# ~ ]
# ~ }
# ~ },
# ~ "reduce": "_count"
# ~ }
# ~ }
# ~ }
# ~ ]
# ~ }
# ~ )
