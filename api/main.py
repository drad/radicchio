#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2022 drad <sa@adercon.com>

import logging

import graypy
from _common.utils import check_databases
from accounts.routes import accounts_router
from auth.routes import auth_router
from config.config import (
    API_BASE_PATH,
    API_PATH,
    APP_LOGLEVEL,
    CDB_URI,
    DEPLOY_ENV,
    GRAYLOG_HOST,
    GRAYLOG_PORT,
    LDAP_HOST,
    LDAP_PORT,
    LOG_TO,
    REDIS_HOST,
    REDIS_PORT,
    cfg,
    couchdb,
)
from core.routes import core_router
from fastapi import FastAPI
from monitors.routes import monitors_router
from notifiers.routes import notifiers_router
from results.routes import results_router
from users.routes import users_router

logger_base = logging.getLogger("default")
logger_base.setLevel(logging.getLevelName(APP_LOGLEVEL))
graylog_handler = graypy.GELFUDPHandler(host=GRAYLOG_HOST, port=GRAYLOG_PORT)
console_handler = logging.StreamHandler()
if "graylog" in LOG_TO:
    logger_base.addHandler(graylog_handler)
if "console" in LOG_TO:
    logger_base.addHandler(console_handler)

logger = logging.LoggerAdapter(
    logging.getLogger("default"),
    {"application_name": cfg.core.name, "application_env": DEPLOY_ENV},
)

logger.info(
    f"{cfg.core.name} - v.{cfg.core.version} ({cfg.core.modified}) - {APP_LOGLEVEL} - {LOG_TO}"
)

logger.info(f"- DB:        {CDB_URI}")
logger.info(f"- LDAP:      {LDAP_HOST}:{LDAP_PORT}")
logger.info(f"- REDIS:     {REDIS_HOST}:{REDIS_PORT}")
logger.info(f"- LogLevel:  {APP_LOGLEVEL}")
logger.info(f"- Base Path: {API_PATH}")

app = FastAPI(
    title=f"{cfg.core.name}",
    description=f"{cfg.core.description}",
    version=f"{cfg.core.version}",
    openapi_url=f"{API_PATH}/openapi.json",
    docs_url=f"{API_PATH}/docs",
    redoc_url=None,
)

app.include_router(
    accounts_router,
    prefix=f"{API_PATH}/accounts",
    tags=["accounts"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    monitors_router,
    prefix=f"{API_PATH}/monitors",
    tags=["monitors"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    results_router,
    prefix=f"{API_PATH}/results",
    tags=["results"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    notifiers_router,
    prefix=f"{API_PATH}/notifiers",
    tags=["notifiers"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    users_router,
    prefix=f"{API_PATH}/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    core_router,
    prefix=f"{API_BASE_PATH}",
    tags=["core"],
    responses={404: {"description": "Not found"}},
)

app.include_router(
    auth_router,
    prefix=f"{API_PATH}/auth",
    tags=["auth"],
    responses={404: {"description": "Not found"}},
)


@app.on_event("startup")
async def app_startup():
    await couchdb.check_credentials()
    await check_databases()


@app.on_event("shutdown")
async def app_shutdown():
    await couchdb.close()
