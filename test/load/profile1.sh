#!/bin/bash

# initial load profiler: used to get initial k8s load profile

# Steps
# - me
# - tasks
# - monitor
# - tasks
# - monitor
# - me
# - monitor
# - tasks

server=https://apps.dradux.com
port=443
base_path="/radicchio/api"
admin_access_key="$(spw -u --get-key="radicchio-prd-admin-access-key")"
#ACCESS_TOKEN='assign this externally'


me() {
    r="$(http --headers GET ${server}:${port}${base_path}/me "Authorization:Bearer ${ACCESS_TOKEN}" | grep HTTP/)"
    echo "${r}"
}

tasks() {
    r="$(http --headers GET ${server}:${port}${base_path}/tasks admin_access_key=${admin_access_key} "Authorization:Bearer ${ACCESS_TOKEN}" | grep HTTP/)"
    echo "${r}"
}
monitor() {
    r="$(http --headers GET ${server}:${port}${base_path}/monitor/ "Authorization:Bearer ${ACCESS_TOKEN}" | grep HTTP/)"
    echo "${r}"
}


echo "PROFILE #1 Startup"
echo "- me #1:      $(me)"
echo "- tasks #1:   $(tasks)"
echo "- monitor #1: $(monitor)"
echo "- tasks #2:   $(tasks)"
echo "- monitor #2: $(monitor)"
echo "- me #2:      $(me)"
echo "- monitor #3: $(monitor)"
echo "- tasks #3:   $(tasks)"
echo "PROFILE #1 Finished"
