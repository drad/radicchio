# README

A rudimentary monitoring and alerting service.

###  Goal  ###
Periodically check a set of sites to ensure they are up, if not, notify.

###  Objective  ###
In order of importance:
- provide site monitoring and alerting
- simple: to use, develop, and maintain
- lightweight: use as few resources as possible (both internal and external)
- maintainable: designed to be maintained
- docker/k8s from the start

### About ###
Radicchio (pronounced rad-ik-e-o) is a bitter, slightly spicy red vegetable that looks similar to red cabbage which is a type of chicory.

###  More Info  ###
See the `docs` directory.
