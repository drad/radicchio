# Usage

- scale number of workers in docker-compose: `docker-compose up --scale worker=2 worker`


- need something to add the db indexes.

### Initial Application Setup Walk Through ###
The following walkthrough assumes you have all components of the app up and working normally (e.g. couchdb, redis, worker, scheduler, api).

- log in through UI as a user with Admin role in ldap
    - this will create the User in radicchio, assign it roles (but not Accounts)
- create a new Account
    - almost everything is tied to an Account
- add Account to the User's list of accounts (preferably as an admin)
    - this gives the user privs on the Account
- add notifiers for the account
- add monitors
    - add the 'test 404` first to make sure everything is working
