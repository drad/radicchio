# Architecture

###  Stack  ###
The application stack is as follows:
- api: backend application
    - fastapi: core application
    - uvicorn/gunicorn: http/wsgi interface to application
    - ldap3: ldap authentication
- scheduler (apscheduler): scheduler interface (singleton)
    - fastapi wrapped app which uses apscheduler's asyncio scheduler to send periodic tasks into arq
    - fastapi exposes several endpoints for the scheduler:
        - list jobs: lists all jobs
        - schedule monitor: schedules a monitor job
        - delete job: deletes a job by id
    - uses UTC timezone
- worker (arq): job queue (worker(s))
- couchdb: database for api
- redis: database for apscheduler (2), arq (1)


###  Authorization Scheme  ###

See the authorization_scheme.ods spreadsheet for details related to authorization including a breakout of privileges by Method for each Model in the application.

###  Links  ###
- [apscheduler](https://apscheduler.readthedocs.io)
- [arq](https://arq-docs.helpmanual.io/)
- [fastapi](https://fastapi.tiangolo.com/)
