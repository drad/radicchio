# design


The monitor flow is as follows:
- monitor created
    - monitor details persisted to db
    - api creates a job on the scheduler to run the monitor given specified time
    - at specified time, scheduler creates an arq task to perform the monitor
    - worker processes monitor task immediately, updating the monitor/result record as appropriate
        - if cert_check is true cert check is also performed (note: this is only performed once per day via the cert_check_flag which is set by an arq cron job)
        - if the monitor finds an issue, notification task(s) are created and placed on the queue
    - worker processes notification task immediately
