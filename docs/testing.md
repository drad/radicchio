#  Testing

- ✓empty url
- invalid urls
    - ✓httm://dradux.com
    - ✓ftp://dradux.com
    - ✓http://dradux.com/invalid
    - ✓https://dradux.com/✓
- ✓redirect url (http:/dradux.com)
- ✓invalid ssl certs
- ✓long urls
- ✓timeout url: ftp://dradux.com
- ✓another timeout: https://httpstat.us/200?sleep=500000
- ✓send any status code you want: https://httpstat.us/404
- ✓example: example.com


###  URLs To Test  ###
    url1 = "https://dradux.com"         # the base
    #url1 = "http://dradux.com"          # redirect
    #url1 = ""                           # empty url
    #url1 = "httm://dradux.com"          # invalid url
    #url1 = "ftp://dradux.com"           # invalie url
    #url1 = "http://dradux.com/invalid"  # invalie url
    #url1 = "https://dradux.com/✓"       # invalie url
    #url1 = "https://apps.dradux.com/"   # invalie url
    #url1 = "https://oc.dradux.com/"     # https cert issue (self signed)
    #url1 = "https://stackoverflow.com/questions/6871016/adding-5-days-to-a-date-in-python"  # long link
    #url1 = "https://docs.python.org/2/library/datetime.html#datetime.timedelta"   # link with param
    #url1 = "http://example.com/"        # example
    #url1 = "https://httpstat.us/404"    # 404
    #url1 = "https://httpstat.us/200?sleep=500000" # timeout (and param)
    #url1 = "ftp://dradux.com"           # timeout
